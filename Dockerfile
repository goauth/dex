FROM golang:alpine as builder



#
# install software
#
RUN \
	echo "*** updating system ***" && \
	apk update && apk upgrade && \
	echo "*** installing system software ***" && \
	apk add --update \
		bash \
		gcc \
		git \
		make \
		sqlite \
		libc-dev \
		curl && \
	rm -rf /var/cache/apk/* /tmp/*

RUN \
	echo "### installing dex" && \
	go get github.com/dexidp/dex | cat -

RUN \
	cd ${GOPATH}/src/github.com/dexidp/dex && \
	make release-binary && \
	make && \
	ls -la



FROM alpine:latest

#
# install software
#
RUN \
	echo "*** updating system ***" && \
	apk update && apk upgrade && \
	echo "*** installing system software ***" && \
	apk add --update \
		ca-certificates \
		sqlite


#
# add files
#
COPY --from=builder /go/src/github.com/dexidp/dex/bin/dex /usr/bin/dex
COPY --from=builder /go/src/github.com/dexidp/dex/web /web
COPY --from=builder /go/src/github.com/dexidp/dex/examples /examples


#
# running tests
#
RUN \
	echo "### running simple tests" && \
	dex version


ENTRYPOINT ["/usr/bin/dex"]
