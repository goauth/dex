version = v0.1
CI_COMMIT_REF_SLUG ?= -
CI_COMMIT_SHA ?= $(shell date +'%Y-%m-%d-%H-%M-%S')
timestamp = $(shell date +'%Y-%m-%d-%H-%M-%S')
tag = $(branch)-$(CI_COMMIT_SHA)
branch = $(CI_COMMIT_REF_SLUG)
ifeq (-,$(CI_COMMIT_REF_SLUG))
	branch = $(shell git rev-parse --abbrev-ref HEAD)
	tag = $(branch)-$(CI_COMMIT_SHA)
endif
ifeq (master,$(CI_COMMIT_REF_SLUG))
	tag = $(version)-$(timestamp)
endif
image = registry.gitlab.com/goauth/dex:$(tag)
curdir = $(shell pwd)


cleanup:
	docker system prune --all --force

docker_build:
	docker build -t $(image) .

docker_push:
	docker build -t $(image) .
	docker push $(image)

changelog_update:
	ls -la
	$(curdir)/update_changelog.sh

debug:
	echo "image: $(image)"

test:
	docker build -t $(image) .
	docker run -i $(image) version
